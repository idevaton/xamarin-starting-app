﻿using System.ComponentModel;
using Xamarin.Forms;
using xamarinapp.ViewModels;

namespace xamarinapp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}