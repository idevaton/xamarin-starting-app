﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using xamarinapp.Models;

namespace xamarinapp.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        //CUSTOM_ADDED_START
        /*adding item object so that we can pass this object to the page*/
        /*the reason to do so is to avoide declarition of many individual attributes and use object attributes instead*/
        private Item _item;
        public Item Item
        {
            get => _item;
            private set
            {
                SetProperty(ref _item, value);
                Title = _item?.Text;
            }
        }
        public ItemDetailViewModel()
        {
            //placeholder
        }
        //CUSTOM_ADDED_END

        private string itemId;
        private string text;
        private string description;
        private string id;

        public string Id
        {
            get => id;
            set => SetProperty(ref id, value);
        }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public string ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                LoadItemId(value);
            }
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                var item = await DataStore.GetItemAsync(itemId);
                Id = itemId;
                Text = item.Text;
                Description = item.Description;

                //CUSTOM_ADDED_START
                var newitem = new Item
                {
                    Id = item.Id,
                    Text = item.Text,
                    Description = item.Description
                };
                Item = newitem;
                //CUSTOM_ADDED_END
            }
            catch (Exception e)
            {
                Debug.WriteLine("Failed to Load Item: " + e.Message.ToString());
            }
        }
    }
}
